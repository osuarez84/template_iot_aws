Este proyecto contiene apuntes y un template funcional para la placa STM32F749 Discovery del proyecto AWS de ejemplo.

Es un ejemplo sobre como utilizar la plataforma AWS para conectar un dispositivo IoT utilizando las certificaciones de AWS
creadas por el Manager del sistema. La conexi�n utiliza un sistema de Publisher-Subscriber para encender y apagar un LED 
de la placa. Se utiliza el OS freeRTOS.

El proyecto contiene tambi�n las credenciales y los diferentes archivos para configurar el "thing".
Se interactua con el equipo mediante un serial terminal.

Ver User Manual para m�s detalles.